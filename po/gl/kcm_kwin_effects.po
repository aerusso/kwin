# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kwin package.
#
# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: kwin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-17 02:32+0000\n"
"PO-Revision-Date: 2019-10-07 10:54+0200\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: package/contents/ui/Effect.qml:92
#, kde-format
msgid ""
"Author: %1\n"
"License: %2"
msgstr ""
"Autor: %1\n"
"Licenza: %2"

#: package/contents/ui/Effect.qml:121
#, kde-format
msgctxt "@info:tooltip"
msgid "Show/Hide Video"
msgstr "Mostrar ou agochar o vídeo"

#: package/contents/ui/Effect.qml:128
#, kde-format
msgctxt "@info:tooltip"
msgid "Configure..."
msgstr "Configurar…"

#: package/contents/ui/main.qml:25
#, kde-format
msgid ""
"Hint: To find out or configure how to activate an effect, look at the "
"effect's settings."
msgstr ""
"Consello: para descubrir ou configurar como activar un efecto, consulte as "
"opcións do efecto."

#: package/contents/ui/main.qml:45
#, kde-format
msgid "Configure Filter"
msgstr "Configurar o filtro"

#: package/contents/ui/main.qml:57
#, kde-format
msgid "Exclude unsupported effects"
msgstr "Excluír os efectos incompatíbeis"

#: package/contents/ui/main.qml:65
#, kde-format
msgid "Exclude internal effects"
msgstr "Excluír os efectos internos"

#: package/contents/ui/main.qml:123
#, kde-format
msgid "Get New Desktop Effects..."
msgstr "Obter novos efectos de escritorio…"

#~ msgid "This module lets you configure desktop effects."
#~ msgstr "Este módulo permítelle configurar os efectos de escritorio."

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Adrian Chaves (Gallaecio)"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "adrian@chaves.io"

#~ msgid "Desktop Effects"
#~ msgstr "Efectos de escritorio"

#~ msgid "Vlad Zahorodnii"
#~ msgstr "Vlad Zahorodnii"

#~ msgid "Download New Desktop Effects"
#~ msgstr "Descargar novos efectos de escritorio"

#~ msgid "Exclude Desktop Effects not supported by the Compositor"
#~ msgstr "Excluír os efectos do escritorio non admitidos polo compositor"

#~ msgid "Search..."
#~ msgstr "Buscar…"
